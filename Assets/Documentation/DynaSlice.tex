\documentclass{article}

\usepackage[margin=0.5in]{geometry}
\usepackage{titlesec}
\usepackage{titling}
\usepackage{setspace}

\usepackage{graphicx}
\usepackage{wrapfig}

\renewcommand{\familydefault}{\sfdefault}

\title{DynaSlice Documentation}
\author{Austin Shahan}

\doublespacing{}

\begin{document}
\maketitle
\tableofcontents
\pagebreak

\section{Introduction}
DynaSlice is a tool for Unity3D that allows the user to slice up a mesh into up to 27 different pieces. These pieces will then automatically position and scale themselves appropriately to fill the space given by the GameObject's scale. The concept was derived from the idea of 9-slice images, but extrapolated into 3 dimensions using mesh data.
The implementation of DynaSlice attempts to follow Unity's own 9-slice image implementation in a faithful manner.

\section{Usage}
To use DynaSlice to slice up a mesh into pieces, create an empty GameObject to be the container of
these pieces. Then add the SlicedMesh component onto the container object. In keeping things simple,
the SlicedMesh component is the only entry point into the tool and its results.

After adding the SlicedMesh component to the container object, supply the Base Mesh property with a MeshFilter component from which it
will pull the shared mesh. \textbf{Please Note:} It is assumed that the GameObject containing the
MeshFilter component also contains a MeshRenderer component. It is from this MeshRenderer that the
SlicedMesh pieces get their materials set.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{Images/SlicedInspector.png}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{Images/SliceExample.png}
\end{figure}
\pagebreak

\subsection{Draw Modes}
DynaSlice's SlicedMesh can be drawn using one of two different draw modes: Sliced and Tiled.

\subsubsection{Sliced}
When the Sliced draw mode is selected, the pieces will stretch to fill their respective region.\\
Corners will stay the same size.\\ Edges will strech along their primary
axis and stay the same size along the other axes.\\ Faces will stretch along their volume's planar axes, but will
stay the same size along its normal axis. See Additional Figure~\ref{fig:resultslice}.


\subsubsection{Tiled}
When the Tiled draw mode is selected, the pieces will tile to fill their respective region.\\
Corners will stay the same size.\\ Edges will tile along their primary axis and stay the same size
along the other axes.\\ Faces will tile along their volume's planar axes, but will stay the same
along its normal axis.\\

\subsubsection{Tiling Modes}
There are two different tiling modes: Continuous and Adaptive. The tiling mode defines where and how
the pieces are tiled.

\noindent\\
When the Continous tile mode is selected, the pieces will repeat evenly within its region. If there
is a tile that will not fully fit in the remaining space, it will be cut to fit within that space.
See Additional Figure~\ref{fig:resultcontinous}.

\noindent\\
When the Adaptive tile mode is selected, the pieces will only tile when they reach the Stretch
Value. Until the piece reaches a new repetition, the existing tiles will stretch to evenly fill the
space between them.
See Additional Figures~\ref{fig:resultadaptive1}~and~\ref{fig:resultadaptive0}.

\section{Updating Slices}
The user can update the slices of the SlicedMesh component in two different ways, through the
inspector, as well as a custom editor window. The user is able to enable slices for the X, Y, and Z
axes as well as update the location of each slice.
\subsection{Inspector}

The inspector for the SlicedMesh component contains three checkboxes for the X, Y, and Z axis
respectively. When a respective axis checkbox is checked, slices are activated for that axis and two float fields are shown displaying
the location of both the negative and positive slice locations. These locations default to
	$\pm~0.75~*$
the bounds extents along that axis.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{Images/SlicedInspector.png}
\end{figure}

\pagebreak
\subsection{Editor Window}

Selecting the `Open Slice Preview Window' button in the inspector brings up the Mesh Slice Window.
The Mesh Slice Window displays the given mesh object in the center of the window, view orientation
buttons in the top left corner, and axis activation checkboxes in the top right corner.\\
Selecting an orientation button changes the camera to view from that direction, similar to the scene
view.\\
The axis checkboxes behave exactly like those in the inspector, enabling or disabling slices for
their respective axis. When a slice axis is enabled, handles will be overlayed onto the mesh that
the user can drag to update the slice locations for that axis. There are also input fields above the
handle for exact input.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{Images/EditorWindow.png}
\end{figure}

\clearpage
\section{Additional Figures}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{Images/Result_Sliced.png}
	\caption{The result of the Sliced draw mode.}
	\label{fig:resultslice}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{Images/Result_Continous.png}
	\caption{The result of the Tiled draw mode with the tile mode set to Continuous.}
	\label{fig:resultcontinous}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{Images/Result_Adaptive1.png}
	\caption{The result of the Tiled draw mode with the tile mode set to Adaptive and a Stretch Value of 1.}
	\label{fig:resultadaptive1}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{Images/Result_Adaptive0.png}
	\caption{The result of the Tiled draw mode with the tile mode set to Adaptive and a Stretch Value of 0.}
	\label{fig:resultadaptive0}
\end{figure}

\end{document}

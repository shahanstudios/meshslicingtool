﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

using DynaSlice.Utils;
using DynaSlice.Mesh;

namespace DynaSlice.Editor {
	using Mesh = UnityEngine.Mesh;
	using DrawMode = SlicedMesh.DrawMode;
	using TileMode = SlicedMesh.TileMode;

	[CustomEditor(typeof(SlicedMesh))]
	public class SlicedMeshEditor : UnityEditor.Editor {
		private SerializedObject so;
		private SerializedProperty propBaseMesh;
		private SerializedProperty propCutGroups;
		private SerializedProperty propDrawMode;
		private SerializedProperty propTileMode;
		private SerializedProperty propStretchValue;

		private SlicedMesh slicedMeshTarget;

		private bool isTiled => propDrawMode.enumValueIndex == (int)DrawMode.Tiled;
		private bool isAdaptive => isTiled && propTileMode.enumValueIndex == (int)TileMode.Adaptive;

		private AnimBool showTileModeFields;
		private AnimBool showAdaptiveField;

		public SlicedMesh SlicedMeshTarget => slicedMeshTarget;
		public Mesh BaseMesh {
			get {
				var meshPropValue = propBaseMesh.objectReferenceValue;
				if (meshPropValue) {
					return ((MeshFilter)meshPropValue).sharedMesh;
				}
				return null;
			}
		}
		public Material[] Materials {
			get {
				var meshPropValue = propBaseMesh.objectReferenceValue;
				if (meshPropValue) {
					return ((MeshFilter)meshPropValue).GetComponent<MeshRenderer>().sharedMaterials;
				}
				return new Material[0];
			}
		}

		public int[] OrientationIndicesInCutGroups {
			get {
				List<CoordinatePlane> orientations = slicedMeshTarget.CutOrientations;

				int enumLength = Enum.GetValues(typeof(CoordinatePlane)).Length;
				int[] indices = new int[enumLength];
				for (int i = 0; i < enumLength; i++) {
					indices[i] = orientations.IndexOf((CoordinatePlane)i);
				}
				return indices;
			}
		}

		private void OnEnable() {
			so = serializedObject;
			propBaseMesh = so.FindProperty("baseMesh");
			propCutGroups = so.FindProperty("cutGroups");
			propDrawMode = so.FindProperty("drawMode");
			propTileMode = so.FindProperty("tileMode");
			propStretchValue = so.FindProperty("stretchValue");

			slicedMeshTarget = (SlicedMesh)target;

			showTileModeFields = new AnimBool(isTiled);
			showTileModeFields.valueChanged.AddListener(Repaint);

			showAdaptiveField = new AnimBool(isAdaptive);
			showAdaptiveField.valueChanged.AddListener(Repaint);
		}

		public override void OnInspectorGUI() {
			so.Update();
			EditorGUILayout.PropertyField(propBaseMesh);
			EditorGUILayout.PropertyField(propDrawMode);

			if (!propBaseMesh.objectReferenceValue) {
				return;
			}

			showTileModeFields.target = isTiled;
			if (EditorGUILayout.BeginFadeGroup(showTileModeFields.faded)) {
				EditorGUILayout.PropertyField(propTileMode);

				showAdaptiveField.target = isAdaptive;
				if (EditorGUILayout.BeginFadeGroup(showAdaptiveField.faded)) {
					EditorGUILayout.PropertyField(propStretchValue);
				}
				EditorGUILayout.EndFadeGroup();
			}
			EditorGUILayout.EndFadeGroup();
			so.ApplyModifiedProperties();

			EditorGUILayout.Space();
			DisplayCutGroups();

			if (GUILayout.Button("Open Slice Preview Window")) {
				SlicedMeshEditorWindow.ShowWindow(this);
			}
		}

		private void DisplayCutGroups() {
			EditorGUILayout.LabelField("Axes Slices", EditorStyles.boldLabel);
			CoordinatePlane[] cutPlanes = Enum.GetValues(typeof(CoordinatePlane)).Cast<CoordinatePlane>().ToArray();
			for (int cgIndex = 0; cgIndex < OrientationIndicesInCutGroups.Length; cgIndex++) {
				int orientationIndex = OrientationIndicesInCutGroups[cgIndex];
				bool containsOrientation = orientationIndex != -1;
				EditorGUILayout.BeginHorizontal();
				EditorGUI.BeginChangeCheck();
				CoordinateAxis cutAxis = ((CoordinatePlane)cgIndex).GetPlaneNormal();
				bool newToggle = GUILayout.Toggle(containsOrientation, new GUIContent($"{cutAxis}:"));
				if (EditorGUI.EndChangeCheck()) {
					UpdateOrientationExistance(newToggle, (CoordinatePlane)cgIndex, orientationIndex);
					orientationIndex = OrientationIndicesInCutGroups[cgIndex];
				}

				if (newToggle) {
					var cuts = slicedMeshTarget.CutGroups[orientationIndex].cuts.ToList();
					var orderedCuts = slicedMeshTarget.CutGroups[orientationIndex].cuts.OrderBy(pi => pi.planeLocation).ToList();
					for (int cutIndex = 0; cutIndex < cuts.Count; cutIndex++) {
						var pi = orderedCuts[cutIndex];
						float planeLocation = pi.planeLocation;
						EditorGUI.BeginChangeCheck();
						planeLocation = EditorGUILayout.DelayedFloatField(planeLocation, EditorStyles.textField);
						planeLocation = Mathf.Clamp(planeLocation, BaseMesh.bounds.min.ValueNotInPlane((CoordinatePlane)cgIndex), BaseMesh.bounds.max.ValueNotInPlane((CoordinatePlane)cgIndex));

						if (EditorGUI.EndChangeCheck()) {
							UpdateCutLocation(orientationIndex, cuts.IndexOf(pi), planeLocation);
						}
					}
				}

				EditorGUILayout.EndHorizontal();
			}
		}

		public void UpdateCutLocation(int cutGroupIndex, int cutIndex, float location) {
			so.Update();
			propCutGroups.GetArrayElementAtIndex(cutGroupIndex).FindPropertyRelative("cuts").GetArrayElementAtIndex(cutIndex).FindPropertyRelative("planeLocation").floatValue = location;
			so.ApplyModifiedProperties();
		}

		public void UpdateOrientationExistance(bool shouldExist, CoordinatePlane orientation, int orientationIndex) {
			float appliedExtent = BaseMesh.bounds.extents.ValueNotInPlane(orientation);
			appliedExtent *= 0.75f;

			so.Update();
			if (shouldExist) {
				propCutGroups.arraySize += 1;
				SerializedProperty newCG = propCutGroups.GetArrayElementAtIndex(propCutGroups.arraySize - 1);
				newCG.FindPropertyRelative("cutOrientation").enumValueIndex = (int)orientation;
				SerializedProperty cuts = newCG.FindPropertyRelative("cuts");
				cuts.arraySize = 2;
				cuts.GetArrayElementAtIndex(0).FindPropertyRelative("planeLocation").floatValue = -appliedExtent;
				cuts.GetArrayElementAtIndex(1).FindPropertyRelative("planeLocation").floatValue = appliedExtent;
			}
			else {
				propCutGroups.DeleteArrayElementAtIndex(orientationIndex);
			}
			so.ApplyModifiedProperties();
		}
	}
}

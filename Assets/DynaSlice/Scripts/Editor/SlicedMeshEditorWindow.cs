﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

using DynaSlice.Utils;
using DynaSlice.Mesh;

namespace DynaSlice.Editor {
	using Mesh = UnityEngine.Mesh;
	using CutGroup = SlicedMesh.CutGroup;
	using PlaneInfo = SlicedMesh.PlaneInfo;

	public class SlicedMeshEditorWindow : EditorWindow {
		private Scene editorScene;
		private SlicedMeshEditor slicedMeshEditor;
		private Vector3 snap = Vector3.one * 0.05f;
		private float size = 0.25f;

		private Mesh BaseMesh => slicedMeshEditor.BaseMesh;
		private Material[] Materials => slicedMeshEditor.Materials;

		private PreviewRenderUtility _previewUtility;
		public PreviewRenderUtility previewUtility {
			get {
				if (_previewUtility == null) {
					_previewUtility = new PreviewRenderUtility();

					_previewUtility.camera.clearFlags = CameraClearFlags.Skybox;
					_previewUtility.camera.orthographic = true;
					_previewUtility.camera.nearClipPlane = 0.3f;
					_previewUtility.camera.farClipPlane = 1000f;
					UpdateCameraOrientation(_previewUtility.camera, BaseMesh.bounds, CoordinatePlane.XY);

					_previewUtility.lights[0].transform.rotation = Quaternion.Euler(50f, -30f, 0);
					_previewUtility.ambientColor = new Color(0.3f, 0.3f, 0.3f, 0);

					GameObject previewGO = new GameObject();
					previewGO.transform.position = Vector3.zero;
					previewGO.AddComponent<MeshFilter>().mesh = BaseMesh;
					previewGO.AddComponent<MeshRenderer>().materials = Materials;
					_previewUtility.AddSingleGO(previewGO);
				}
				return _previewUtility;
			}
		}
		private Camera previewCamera => previewUtility.camera;
		private CoordinatePlane currentOrientation;

		private Dictionary<CoordinatePlane, Color> _coordinatePlaneColorMap;
		private Dictionary<CoordinatePlane, Color> coordinatePlaneColorMap {
			get {
				if (_coordinatePlaneColorMap == null) {
					_coordinatePlaneColorMap = new Dictionary<CoordinatePlane, Color>();
					_coordinatePlaneColorMap.Add(CoordinatePlane.YZ, Handles.xAxisColor);
					_coordinatePlaneColorMap.Add(CoordinatePlane.XY, Handles.zAxisColor);
					_coordinatePlaneColorMap.Add(CoordinatePlane.XZ, Handles.yAxisColor);
				}
				return _coordinatePlaneColorMap;
			}
		}

		public static void ShowWindow(SlicedMeshEditor editor) {
			var window = GetWindow<SlicedMeshEditorWindow>("Mesh Slice Window");
			window.slicedMeshEditor = editor;
		}

		private void OnDisable() {
			if (_previewUtility != null) {
				_previewUtility.Cleanup();
			}
		}

		private void UpdateCameraOrientation(Camera camera, Bounds bounds, CoordinatePlane orientationPlane) {
			Bounds b = new Bounds(bounds.center, bounds.size);
			b.Expand(1f);
			Vector3 chosen = orientationPlane == CoordinatePlane.XY ? b.min : b.max;
			Vector3 position = Vector2.zero.ConstructPlanarVector3(chosen.ValueNotInPlane(orientationPlane), orientationPlane);

			camera.transform.position = position;
			camera.transform.rotation = Quaternion.LookRotation(-_previewUtility.camera.transform.position, Vector3.up);
			UpdateCameraSize(camera, bounds, orientationPlane);

			currentOrientation = orientationPlane;
		}
		private void UpdateCameraSize(Camera camera, Bounds bounds, CoordinatePlane orientationPlane) {
			Bounds b = new Bounds(bounds.center, bounds.size);
			b.Expand(1f);

			Vector2 planarExtents = b.extents.InPlane(orientationPlane);
			float extentsMax = planarExtents.MaxComponent();
			float extentsMin = planarExtents.MinComponent();

			float aspect = extentsMin / extentsMax;

			camera.orthographicSize = extentsMax * aspect;
		}

		private void OnGUI() {
			Rect previewRect = GetPreviewRect();
			DrawScene(previewRect);

			Handles.SetCamera(previewRect, previewUtility.camera);
			CutGroup[] groups = slicedMeshEditor.SlicedMeshTarget.CutGroups;
			for (int cgIndex = 0; cgIndex < groups.Length; cgIndex++) {
				CutGroup group = groups[cgIndex];
				if (group.cutOrientation == currentOrientation) {
					continue;
				}

				for (int cutIndex = 0; cutIndex < group.cuts.Length; cutIndex++) {
					PlaneInfo info = group.cuts[cutIndex];

					DrawHandle(info.planeLocation, group.cutOrientation,
						newLocation =>
						{
							slicedMeshEditor.UpdateCutLocation(cgIndex, cutIndex, newLocation);
						});
				}
			}
			DrawCameraButtons();
			DrawAxesCheckboxes();
		}
		private Rect GetPreviewRect() {
			Rect previewRect;
			float height = position.height;
			int widthFactor = 1024;
			if (position.width < widthFactor) {
				previewRect = new Rect(0, 0, position.width + (widthFactor - position.width), height);
			}
			else {
				previewRect = new Rect(0, 0, position.width, height);
			}
			return previewRect;
		}
		private void DrawScene(Rect r) {
			previewUtility.BeginPreview(r, new GUIStyle());
			previewUtility.Render();
			previewUtility.EndAndDrawPreview(r);
		}
		private void DrawHandle(float location, CoordinatePlane cutOrientation, Action<float> OnLocationChangeFunc) {
			Vector3 position = Vector2.zero.ConstructPlanarVector3(location, cutOrientation);
			float handleSize = HandleUtility.GetHandleSize(position) * size;

			Handles.color = coordinatePlaneColorMap[cutOrientation];

			DrawPlanarRectangle(location, cutOrientation);
			EditorGUI.BeginChangeCheck();
			Vector3 newPosition = Handles.FreeMoveHandle(position, Quaternion.LookRotation(Vector3.right), handleSize, snap, Handles.RectangleHandleCap);
			newPosition = newPosition.ClampComponents(-BaseMesh.bounds.extents, BaseMesh.bounds.extents);
			float newLocation = newPosition.ValueNotInPlane(cutOrientation);
			if (EditorGUI.EndChangeCheck()) {
				OnLocationChangeFunc(newLocation);
			}

			Vector2 cameraPoint = previewCamera.WorldToScreenPoint(newPosition);
			cameraPoint.y = this.position.height - cameraPoint.y;
			Handles.BeginGUI();

			Vector2 inputSize = new Vector2(100, 100);
			GUILayout.BeginArea(new Rect(cameraPoint.x - inputSize.x / 2f, cameraPoint.y - inputSize.y / 2f, inputSize.x, inputSize.y));
			EditorGUI.BeginChangeCheck();
			newLocation = EditorGUILayout.DelayedFloatField(newLocation, EditorStyles.textField);
			if (EditorGUI.EndChangeCheck()) {
				OnLocationChangeFunc(newLocation);
			}
			GUILayout.EndArea();
			Handles.EndGUI();
		}
		private void DrawPlanarRectangle(float location, CoordinatePlane cutOrientation) {
			Bounds b = new Bounds(Vector2.zero.ConstructPlanarVector3(location, cutOrientation), BaseMesh.bounds.size.InPlane(cutOrientation).ConstructPlanarVector3(0, cutOrientation));
			Vector2 minIP = b.min.InPlane(cutOrientation);
			Vector2 maxIP = b.max.InPlane(cutOrientation);
			Vector3[] verts = new Vector3[] {
			new Vector2(minIP.x, minIP.y).ConstructPlanarVector3(location, cutOrientation),
			new Vector2(maxIP.x, minIP.y).ConstructPlanarVector3(location, cutOrientation),
			new Vector2(maxIP.x, maxIP.y).ConstructPlanarVector3(location, cutOrientation),
			new Vector2(minIP.x, maxIP.y).ConstructPlanarVector3(location, cutOrientation),
		};
			Color rectInner = new Color(Handles.color.r, Handles.color.g, Handles.color.b, 0.1f);
			Handles.DrawSolidRectangleWithOutline(verts, rectInner, Handles.color);
		}

		private void DrawCameraButtons() {
			Handles.BeginGUI();
			GUILayout.BeginArea(new Rect(10, 10, 100, 100));
			if (GUILayout.Button("Back")) {
				UpdateCameraOrientation(previewCamera, BaseMesh.bounds, CoordinatePlane.XY);
			}
			if (GUILayout.Button("Top")) {
				UpdateCameraOrientation(previewCamera, BaseMesh.bounds, CoordinatePlane.XZ);
			}
			if (GUILayout.Button("Right")) {
				UpdateCameraOrientation(previewCamera, BaseMesh.bounds, CoordinatePlane.YZ);
			}
			GUILayout.EndArea();
			Handles.EndGUI();
		}

		private float areaWidth = 50;
		private float areaHeight = 60;
		private void DrawAxesCheckboxes() {
			Handles.BeginGUI();
			GUILayout.BeginArea(new Rect(position.width - areaWidth - 10, 10, areaWidth, areaHeight), EditorStyles.helpBox);
			int[] orientationIndicesInCutGroups = slicedMeshEditor.OrientationIndicesInCutGroups;
			for (int i = 0; i < orientationIndicesInCutGroups.Length; i++) {
				int orientationIndex = orientationIndicesInCutGroups[i];
				bool containsOrientation = orientationIndex != -1;
				EditorGUI.BeginChangeCheck();
				CoordinateAxis cutAxis = ((CoordinatePlane)i).GetPlaneNormal();
				bool newToggle = GUILayout.Toggle(containsOrientation, new GUIContent($"{cutAxis}"));
				if (EditorGUI.EndChangeCheck()) {
					slicedMeshEditor.UpdateOrientationExistance(newToggle, (CoordinatePlane)i, orientationIndex);
				}
			}
			GUILayout.EndArea();
			Handles.EndGUI();
		}
	}
}

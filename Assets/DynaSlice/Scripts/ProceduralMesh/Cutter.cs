﻿using System.Collections.Generic;
using UnityEngine;

namespace DynaSlice.Procedural {
	using Mesh = UnityEngine.Mesh;

	public class Cutter {

		public (GeneratedMesh, GeneratedMesh) Cut(Mesh originalMesh, Plane plane) {
			GeneratedMesh positiveMesh = new GeneratedMesh();
			GeneratedMesh negativeMesh = new GeneratedMesh();

			int[] submeshIndices;
			int triangleIndexA, triangleIndexB, triangleIndexC;

			for (int i = 0; i < originalMesh.subMeshCount; i++) {
				submeshIndices = originalMesh.GetTriangles(i);

				for (int j = 0; j < submeshIndices.Length; j += 3) {
					triangleIndexA = submeshIndices[j    ];
					triangleIndexB = submeshIndices[j + 1];
					triangleIndexC = submeshIndices[j + 2];

					MeshTriangle currentTriangle = GetTriangle(originalMesh, triangleIndexA, triangleIndexB, triangleIndexC, i); 

					bool triangleAPosSide = plane.GetSide(originalMesh.vertices[triangleIndexA]);
					bool triangleBPosSide = plane.GetSide(originalMesh.vertices[triangleIndexB]);
					bool triangleCPosSide = plane.GetSide(originalMesh.vertices[triangleIndexC]);

					if (triangleAPosSide && triangleBPosSide && triangleCPosSide) {
						positiveMesh.AddTriangle(currentTriangle);
					}
					else if (!triangleAPosSide && !triangleBPosSide && !triangleCPosSide) {
						negativeMesh.AddTriangle(currentTriangle);
					}
					else {
						List<bool> isPosSide = new List<bool>() { triangleAPosSide, triangleBPosSide, triangleCPosSide };
						CutTriangle(plane, currentTriangle, isPosSide, positiveMesh, negativeMesh);
					}
				}
			}

			return (positiveMesh, negativeMesh);
		}

		private MeshTriangle GetTriangle(Mesh originalMesh, int triangleIndexA, int triangleIndexB, int triangleIndexC, int submeshIndex) {
			Vector3[] vertices = new Vector3[3];
			vertices[0] = originalMesh.vertices[triangleIndexA];
			vertices[1] = originalMesh.vertices[triangleIndexB];
			vertices[2] = originalMesh.vertices[triangleIndexC];

			Vector3[] normals = new Vector3[3];
			normals[0] = originalMesh.normals[triangleIndexA];
			normals[1] = originalMesh.normals[triangleIndexB];
			normals[2] = originalMesh.normals[triangleIndexC];

			Vector2[] uvs = new Vector2[3];
			uvs[0] = originalMesh.uv[triangleIndexA];
			uvs[1] = originalMesh.uv[triangleIndexB];
			uvs[2] = originalMesh.uv[triangleIndexC];

			MeshTriangle triangle = new MeshTriangle(vertices, normals, uvs, submeshIndex);
			return triangle;
		}

		private void CutTriangle(Plane plane, MeshTriangle triangle, List<bool> isPosSide, GeneratedMesh positiveMesh, GeneratedMesh negativeMesh) {
			int isolatedPointIndex = 0;
			if (isPosSide[0] != isPosSide[1]) {
				isolatedPointIndex = isPosSide[0] != isPosSide[2] ? 0 : 1;
			}
			else {
				isolatedPointIndex = 2;
			}

			int prevPointIndex = isolatedPointIndex - 1;
			prevPointIndex = (prevPointIndex == -1) ? 2 : prevPointIndex;

			int nextPointIndex = isolatedPointIndex + 1;
			nextPointIndex = (nextPointIndex == 3) ? 0 : nextPointIndex;

			(Vector3 newVertPrev, Vector3 newNormPrev, Vector2 newUVPrev) = GetIntersection(plane, isolatedPointIndex, prevPointIndex, triangle);
			(Vector3 newVertNext, Vector3 newNormNext, Vector2 newUVNext) = GetIntersection(plane, isolatedPointIndex, nextPointIndex, triangle);

			MeshTriangle isolatedTriangle = new MeshTriangle(
				new Vector3[] { triangle.Vertices[isolatedPointIndex], newVertNext, newVertPrev },
				new Vector3[] { triangle.Normals[isolatedPointIndex], newNormNext, newNormPrev },
				new Vector2[] { triangle.UVs[isolatedPointIndex], newUVNext, newUVPrev },
				triangle.SubmeshIndex
			);
			(isPosSide[isolatedPointIndex] ? positiveMesh : negativeMesh).AddTriangle(isolatedTriangle);

			MeshTriangle newPrevTriangle = new MeshTriangle(
				new Vector3[] { triangle.Vertices[prevPointIndex], newVertPrev, newVertNext },
				new Vector3[] { triangle.Normals[prevPointIndex], newNormPrev, newNormNext },
				new Vector2[] { triangle.UVs[prevPointIndex], newUVPrev, newUVNext },
				triangle.SubmeshIndex
			);
			(isPosSide[prevPointIndex] ? positiveMesh : negativeMesh).AddTriangle(newPrevTriangle);

			MeshTriangle newNextTriangle = new MeshTriangle(
				new Vector3[] { triangle.Vertices[nextPointIndex], triangle.Vertices[prevPointIndex], newVertNext },
				new Vector3[] { triangle.Normals[nextPointIndex], triangle.Normals[prevPointIndex], newNormNext },
				new Vector2[] { triangle.UVs[nextPointIndex], triangle.UVs[prevPointIndex], newUVNext },
				triangle.SubmeshIndex
			);
			(isPosSide[nextPointIndex] ? positiveMesh : negativeMesh).AddTriangle(newNextTriangle);
		}

		private (Vector3, Vector3, Vector3) GetIntersection(Plane plane, int aIndex, int bIndex, MeshTriangle triangle) {
			var vertices = triangle.Vertices; var normals = triangle.Normals; var uvs = triangle.UVs;
			Vector3 a = vertices[aIndex];
			Vector3 b = vertices[bIndex];
			Vector3 dirVec = b - a;
			plane.Raycast(new Ray(a, dirVec.normalized), out float distance);

			float normalizedDistance = distance / dirVec.magnitude;
			Vector3 vertPos = Vector3.Lerp(a, b, normalizedDistance);
			Vector3 normalPos = Vector3.Lerp(normals[aIndex], normals[bIndex], normalizedDistance);
			Vector2 uvPos = Vector2.Lerp(uvs[aIndex], uvs[bIndex], normalizedDistance);

			return (vertPos, normalPos, uvPos);
		}
	}
}

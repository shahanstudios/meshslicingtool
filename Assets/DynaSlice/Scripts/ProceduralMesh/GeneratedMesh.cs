﻿using System.Collections.Generic;
using UnityEngine;

namespace DynaSlice.Procedural {
	using Mesh = UnityEngine.Mesh;

	public class GeneratedMesh {
		List<Vector3> vertices = new List<Vector3>();
		List<Vector3> normals = new List<Vector3>();
		List<Vector2> uvs = new List<Vector2>();
		List<List<int>> submeshIndices = new List<List<int>>();

		public void AddTriangle(MeshTriangle triangle) {
			int currentVertexCount = vertices.Count;

			vertices.AddRange(triangle.Vertices);
			normals.AddRange(triangle.Normals);
			uvs.AddRange(triangle.UVs);

			if (submeshIndices.Count < triangle.SubmeshIndex + 1) {
				for (int i = submeshIndices.Count; i < triangle.SubmeshIndex + 1; i++) {
					submeshIndices.Add(new List<int>());
				}
			}

			for (int i = 0; i < 3; i++) {
				submeshIndices[triangle.SubmeshIndex].Add(currentVertexCount + i);
			}
		}

		public Mesh CreateMesh() {
			Mesh mesh = new Mesh();
			mesh.vertices = vertices.ToArray();
			mesh.uv = uvs.ToArray();
			mesh.subMeshCount = submeshIndices.Count;
			for (int i = 0; i < submeshIndices.Count; i++) {
				mesh.SetTriangles(submeshIndices[i], i);
			}
			mesh.normals = normals.ToArray();
			return mesh;
		}
	}

	public class MeshTriangle {
		private List<Vector3> vertices = new List<Vector3>();
		private List<Vector3> normals = new List<Vector3>();
		private List<Vector2> uvs = new List<Vector2>();
		private int submeshIndex;

		public List<Vector3> Vertices => vertices;	
		public List<Vector3> Normals => normals;	
		public List<Vector2> UVs => uvs;	
		public int SubmeshIndex => submeshIndex;

		public MeshTriangle(Vector3[] vertices, Vector3[] normals, Vector2[] uvs, int submeshIndex) {
			Clear();

			this.vertices.AddRange(vertices);
			this.normals.AddRange(normals);
			this.uvs.AddRange(uvs);
			this.submeshIndex = submeshIndex;
		}

		public void Clear() {
			vertices.Clear();
			normals.Clear();
			uvs.Clear();
			submeshIndex = 0;
		}

		public void Flip() {
			vertices.Reverse();
			normals.Reverse();
			uvs.Reverse();
		}
	}
}

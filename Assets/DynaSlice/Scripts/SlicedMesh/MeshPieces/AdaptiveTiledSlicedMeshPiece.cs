﻿using UnityEngine;

using DynaSlice.Utils;

namespace DynaSlice.Mesh {
	public class AdaptiveTiledSlicedMeshPiece : TiledSlicedMeshPiece {
		private float stretchValue = 0.5f;
		private Vector3 numberOfPiecesPerDirection;

		public void SetStretchValue(float stretchValue) {
			this.stretchValue = stretchValue;
		}

		protected override Vector3 GetNumberOfPiecesPerDirection(Vector3 scale) {
			float stretchFactor(float x) {
				return Mathf.Max(Mathf.Ceil(x - stretchValue), 1);
			};
			Vector3 numPieces = GetNumberOfPiecesPerDirection(scale, stretchFactor);
			numberOfPiecesPerDirection = numPieces;
			return numPieces;
		}

		protected override Vector3 GetTileScale(Vector3 scale) {
			Vector3 spaceToFill = GetSpaceToFill(scale);
			Vector3 spacePerPiece = spaceToFill.InverseScale(numberOfPiecesPerDirection);		
			Vector3 newScale = spacePerPiece.InverseScale(Vector3.Scale(bounds.size, scale));

			return newScale;
		}
	}
}

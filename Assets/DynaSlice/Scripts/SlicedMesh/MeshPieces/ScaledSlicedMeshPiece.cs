﻿using UnityEngine;

using DynaSlice.Utils;

namespace DynaSlice.Mesh {
	public class ScaledSlicedMeshPiece : SlicedMeshPiece {
		protected override void Start() {
			base.Start();
			gameObject.AddComponent<MeshFilter>().mesh = pieceMesh;
			gameObject.AddComponent<MeshRenderer>().materials = materials;
		}

		protected override void UpdatePieceScale(Vector3 scale) {
			UpdatePieceLocalScaleAndPosition(scale, GetNewPieceScale(scale));
		}

		private Vector3 GetNewPieceScale(Vector3 scale) {
			Vector3 spaceToFill = GetSpaceToFill(scale);
			Vector3 newScale = spaceToFill.InverseScale(Vector3.Scale(bounds.size, scale));

			return newScale;
		}
	}
}

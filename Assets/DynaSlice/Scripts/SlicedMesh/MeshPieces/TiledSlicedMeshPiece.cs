﻿using System;
using System.Collections.Generic;
using UnityEngine;

using DynaSlice.Utils;
using DynaSlice.Procedural;

namespace DynaSlice.Mesh {
	using Mesh = UnityEngine.Mesh;
	using PlaneInfo = SlicedMesh.PlaneInfo;

	public class TiledSlicedMeshPiece : SlicedMeshPiece {
		private List<List<GameObject>> tiles = new List<List<GameObject>>();
		private CoordinatePlane[] cutPlanes => spreadAxes.GetCoordinatePlaneFromGivenNormal();

		private Cutter cutter = new Cutter();

		protected override void Start() {
			base.Start();

			UpdatePieceScale(transform.parent.localScale);
		}

		protected override void UpdatePieceScale(Vector3 scale) {
			if (spreadAxes == CoordinateAxis.None && leftoverCutNormals == CoordinateAxis.None) {
				return;
			}
			UpdatePieceLocalScaleAndPosition(scale, Vector3.one);

			int oldCount = tiles.Count;
			Vector2 numPieces = GetNumberOfPiecesPerDirection(scale).ApplyVectorAlongAxis(spreadAxes);
			if (numPieces.x < tiles.Count) {
				for (int x = tiles.Count - 1; x >= numPieces.x; x--) {
					List<GameObject> yTiles = tiles[x];
					foreach (var tile in yTiles) {
						Destroy(tile);
					}
					tiles.RemoveAt(x);
				}
			}
			else if (numPieces.x > tiles.Count) {
				for (int x = tiles.Count; x < numPieces.x; x++) {
					tiles.Insert(x, new List<GameObject>((int)numPieces.y));
				}
			}

			for (int x = 0; x < tiles.Count; x++) {
				List<GameObject> yTiles = tiles[x];
				UpdateYTiles(yTiles, numPieces.y, x, scale, numPieces);
			}
		}

		private void UpdateYTiles(List<GameObject> yTiles, float numPiecesY, int xIndex, Vector3 scale, Vector2 count) {
			if (numPiecesY < yTiles.Count) {
				GameObject tileToRemove;
				for (int y = yTiles.Count - 1; y >= numPiecesY; y--) {
					tileToRemove = yTiles[y];	
					Destroy(tileToRemove);
					yTiles.RemoveAt(y);
				}
			}
			else if (numPiecesY > yTiles.Count) {
				GameObject newTile;
				for (int y = yTiles.Count; y < numPiecesY; y++) {
					newTile = new GameObject();
					newTile.AddComponent<MeshFilter>().mesh = pieceMesh;
					newTile.AddComponent<MeshRenderer>().materials = materials;
					newTile.transform.parent = this.transform;
					newTile.name = $"{xIndex}, {y}";
					Vector3 tileScale = GetAppliedTileScale(scale);
					newTile.transform.localScale = tileScale;
					newTile.transform.localPosition = GetTilePosition(tileScale, y, xIndex, count);

					yTiles.Insert(y, newTile);
				}
			}

			void UpdateExistingYTiles(Mesh baseMesh) {
				for (int y = 0; y < yTiles.Count; y++) {
					GameObject existingTile = yTiles[y];
					Vector3 tileScale = GetAppliedTileScale(scale);
					existingTile.transform.localScale = tileScale;
					existingTile.transform.localPosition = GetTilePosition(tileScale, y, xIndex, count);
					Mesh newMesh = baseMesh;

					float leftoverSpaceY = count.y - y;
					if (leftoverSpaceY < 1f) {
						leftoverSpaceY = newMesh.bounds.size.ApplyVectorAlongAxis(spreadAxes).y * leftoverSpaceY - newMesh.bounds.extents.ApplyVectorAlongAxis(spreadAxes).y;
						(GeneratedMesh negativeMesh, GeneratedMesh positiveMesh) = cutter.Cut(newMesh, GetCutPlane(leftoverSpaceY, cutPlanes[1]));
						newMesh = positiveMesh.CreateMesh();
					}

					existingTile.GetComponent<MeshFilter>().mesh = newMesh;
				}
			}

			float leftoverSpaceX = count.x - xIndex;
			if (leftoverSpaceX < 1f) {
				leftoverSpaceX = pieceMesh.bounds.size.ApplyVectorAlongAxis(spreadAxes).x * leftoverSpaceX - pieceMesh.bounds.extents.ApplyVectorAlongAxis(spreadAxes).x;
				(GeneratedMesh negativeMesh, GeneratedMesh positiveMesh) = cutter.Cut(pieceMesh, GetCutPlane(leftoverSpaceX, cutPlanes[0]));

				UpdateExistingYTiles(positiveMesh.CreateMesh());
			}
			else {
				UpdateExistingYTiles(pieceMesh);
			}
		}
		protected virtual Vector3 GetTileScale(Vector3 scale) {
			Vector3 invScale = scale.InvertComponents();
			return invScale;
		}
		private Vector3 GetAppliedTileScale(Vector3 scale) {
			Vector3 newScale = GetTileScale(scale);

			Vector3 parentLocalScale = this.transform.localScale.ApplyVectorAlongAxis(spreadAxes);
			parentLocalScale = ((Vector2)parentLocalScale).ConstructAxialVector3(spreadAxes);

			return new Vector3(
				parentLocalScale.x == 1 ? newScale.x : 1f,
				parentLocalScale.y == 1 ? newScale.y : 1f,
				parentLocalScale.z == 1 ? newScale.z : 1f
			);
		}
		protected virtual Vector3 GetTilePosition(Vector3 tileScale, int y, int xIndex, Vector2 count) {
			Vector3 scaledSize = Vector3.Scale(bounds.size, tileScale);
			Vector2 scaledTileSize = scaledSize.ApplyVectorAlongAxis(spreadAxes);
			Vector2 halfTileSize = scaledTileSize / 2f;

			Vector3 axialVector = Vector2.Scale(new Vector2(xIndex, y), scaledTileSize).ConstructAxialVector3(spreadAxes);
			Vector3 axialCount = count.ConstructAxialVector3(spreadAxes);
			Vector3 axialHalfSize = halfTileSize.ConstructAxialVector3(spreadAxes);
			return axialVector - axialHalfSize.ScaleComponents(axialCount) + axialHalfSize;
		}

		protected virtual Vector3 GetNumberOfPiecesPerDirection(Vector3 scale) {
			return GetNumberOfPiecesPerDirection(scale, x => x);
		}
		protected virtual Vector3 GetNumberOfPiecesPerDirection(Vector3 scale, Func<float, float> applyFunc) {
			Vector3 spaceToFill = GetSpaceToFill(scale);
			Vector3 numberPerDirection = spaceToFill.InverseScale(bounds.size);
			Vector3 appliedNumberPerDirection = GetAppliedVectorComponents(numberPerDirection, Vector3.one);

			float epsilon = 0.001f;
			appliedNumberPerDirection = new Vector3(
					Mathf.Abs(appliedNumberPerDirection.x) < epsilon ? 0 : appliedNumberPerDirection.x,
					Mathf.Abs(appliedNumberPerDirection.y) < epsilon ? 0 : appliedNumberPerDirection.y,
					Mathf.Abs(appliedNumberPerDirection.z) < epsilon ? 0 : appliedNumberPerDirection.z);

			appliedNumberPerDirection = new Vector3(
					applyFunc(appliedNumberPerDirection.x),
					applyFunc(appliedNumberPerDirection.y),
					applyFunc(appliedNumberPerDirection.z));

			return appliedNumberPerDirection;
		}

		private Plane GetCutPlane(float planeLoc, CoordinatePlane normalPlane) {
			PlaneInfo planeInfo = new PlaneInfo();
			planeInfo.planeLocation = planeLoc;
			
			return planeInfo.ToPlane(normalPlane);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using DynaSlice.Utils;
using DynaSlice.Procedural;

namespace DynaSlice.Mesh {
	using Mesh = UnityEngine.Mesh;

	public class SlicedMesh : MonoBehaviour {
		[SerializeField] private MeshFilter baseMesh = null;
		[SerializeField] private CutGroup[] cutGroups = new CutGroup[0];

		[SerializeField] private DrawMode drawMode = DrawMode.Sliced;
		[SerializeField] private TileMode tileMode = TileMode.Continuous;
		[SerializeField, Range(0,1)] private float stretchValue = 0.5f;

		public CutGroup[] CutGroups => cutGroups;
		public List<CoordinatePlane> CutOrientations => cutGroups.Select(cg => cg.cutOrientation).ToList();

		private new Transform transform;
		private Vector3 previousLocalScale;

		public Bounds BaseMeshBounds => baseMesh.sharedMesh.bounds;
		public event Action<Vector3> ScaleChangedEvent;

		public void Awake() {
			Mesh mesh = baseMesh.sharedMesh;
			Material[] mats = baseMesh.GetComponent<MeshRenderer>().sharedMaterials;

			transform = GetComponent<Transform>();
			previousLocalScale = transform.localScale;

			Cutter cutter = new Cutter();

			cutGroups = CoordinatePlaneUtils.CoordinatePlanes.Select(cp => CutOrientations.IndexOf(cp)).Where(i => i != -1).Select(i => this.cutGroups[i]).ToArray();
			List<VolumeInfo> volumes = GetCutVolumes();

			List<Mesh>[] meshesToProcessForGroup = new List<Mesh>[cutGroups.Length + 1];
			for (int i = 0; i < meshesToProcessForGroup.Length; i++) {
				meshesToProcessForGroup[i] = new List<Mesh>();
			}
			meshesToProcessForGroup[0].Add(mesh);

			int numPiecesToProcessPerOrientation = 1;
			for (int g = 0; g < cutGroups.Length; g++) {
				CutGroup grouping = cutGroups[g];
				
				List<Mesh> currentGroupMeshes = meshesToProcessForGroup[g];
				Queue<Mesh> intermediateCuts = new Queue<Mesh>();
				Plane plane = grouping.GetPlane(0);
				for (int im = 0; im < currentGroupMeshes.Count; im++) {
					Mesh curProcessingPiece = currentGroupMeshes[im];
					(GeneratedMesh positiveMesh, GeneratedMesh negativeMesh) = cutter.Cut(curProcessingPiece, plane);

					var pm = positiveMesh.CreateMesh(); // pm.name = $"Positive Mesh {g} {c} {cnt}";
					var nm = negativeMesh.CreateMesh(); // nm.name = $"Negative Mesh {g} {c} {cnt}";

					intermediateCuts.Enqueue(pm);
					meshesToProcessForGroup[g + 1].Add(nm);
				}

				for (int c = 1; c < grouping.cuts.Length; c++) {
					plane = grouping.GetPlane(c);

					for (int i = 0; i < numPiecesToProcessPerOrientation; i++) {
						Mesh curProcessingPiece = intermediateCuts.Dequeue();
						(GeneratedMesh positiveMesh, GeneratedMesh negativeMesh) = cutter.Cut(curProcessingPiece, plane);

						var pm = positiveMesh.CreateMesh(); // pm.name = $"Positive Mesh {g} {c} {cnt}";
						var nm = negativeMesh.CreateMesh(); // nm.name = $"Negative Mesh {g} {c} {cnt}";

						intermediateCuts.Enqueue(pm);
						meshesToProcessForGroup[g + 1].Add(nm);
					}
				}
				meshesToProcessForGroup[g + 1].AddRange(intermediateCuts);
				numPiecesToProcessPerOrientation *= cutGroups[g].cuts.Length + 1;
			}
			List<Mesh> pieces = new List<Mesh>(meshesToProcessForGroup.Last());

			CreateMeshObjectFromPieces(pieces, volumes, mats, this.gameObject);
		}

		private GameObject CreateMeshObjectFromPieces(List<Mesh> pieces, List<VolumeInfo> volumes, Material[] mats, GameObject parent = null) {
			GameObject container = parent ? parent : new GameObject();
			List<int> cornerIndices = new List<int>();
			if (drawMode != DrawMode.Sliced && pieces.Count == 27) {
				cornerIndices = new List<int>() { 0, 2, 6, 8, 18, 20, 24, 26 };
			}
			for (int i = 0; i < pieces.Count; i++) {
				Mesh piece = pieces[i];
				if (piece.bounds.size.Equals(Vector3.zero)) {
					continue;
				}
				GameObject meshObject = new GameObject();
				meshObject.name = piece.name.Length > 0 ? piece.name : $"Mesh Piece {i}";
				meshObject.transform.parent = container.transform;
				VolumeInfo volumeInfo = volumes[i];

				Material[] pieceMats = new Material[piece.subMeshCount];
				for (int pm = 0; pm < pieceMats.Length; pm++) {
					pieceMats[pm] = mats[pm];
				}

				Type componentType = (drawMode == DrawMode.Sliced || cornerIndices.Contains(i)) ?
					typeof(ScaledSlicedMeshPiece) :
					(tileMode == TileMode.Continuous) ? typeof(TiledSlicedMeshPiece) : typeof(AdaptiveTiledSlicedMeshPiece);
				((SlicedMeshPiece)meshObject.AddComponent(componentType)).Init(piece, pieceMats, volumeInfo.bounds, volumeInfo.spreadAxes, CutOrientations);
				if (componentType == typeof(AdaptiveTiledSlicedMeshPiece)) {
					meshObject.GetComponent<AdaptiveTiledSlicedMeshPiece>().SetStretchValue(this.stretchValue);
				}
			}
			return container;
		}
		
		private CutGroup[] GetAllMeshCuts() {
			CoordinatePlane[] cps = CoordinatePlaneUtils.CoordinatePlanes.ToArray();
			CutGroup[] meshBoundsCutGroups = new CutGroup[cps.Length];
			for (int i = 0; i < meshBoundsCutGroups.Length; i++) {
				CoordinatePlane cp = cps[i];
				meshBoundsCutGroups[i].cutOrientation = cp;
				PlaneInfo[] boundsCuts = new PlaneInfo[2];
				boundsCuts[0] = new PlaneInfo() { planeLocation = BaseMeshBounds.min.ValueNotInPlane(cp) };
				boundsCuts[1] = new PlaneInfo() { planeLocation = BaseMeshBounds.max.ValueNotInPlane(cp) };
				meshBoundsCutGroups[i].cuts = boundsCuts;
			}

			CutGroup[] fullCutGroups = new CutGroup[cps.Length];
			int[] cutGroupIndices = cps.Select(cp => CutOrientations.IndexOf(cp)).ToArray();

			for (int i = 0; i < fullCutGroups.Length; i++) {
				CoordinatePlane cp = cps[i];
				fullCutGroups[i].cutOrientation = cp;
				
				int existingCGIndex = cutGroupIndices[i];
				PlaneInfo[] existingCuts = (existingCGIndex == -1) ? new PlaneInfo[0] : cutGroups[existingCGIndex].cuts;
				PlaneInfo[] fullCuts = new PlaneInfo[existingCuts.Length + 2];
				fullCuts[0] = meshBoundsCutGroups[i].cuts[0];
				for (int j = 0; j < existingCuts.Length; j++) {
					fullCuts[1 + j] = existingCuts[j];
				}
				fullCuts[fullCuts.Length - 1] = meshBoundsCutGroups[i].cuts[1];

				fullCutGroups[i].cuts = fullCuts;
			}

			return fullCutGroups;
		}
		private List<VolumeInfo> GetCutVolumes() {
			CutGroup[] allCuts = GetAllMeshCuts();

			CoordinatePlane aPlane = allCuts[2].cutOrientation;
			CoordinatePlane bPlane = allCuts[1].cutOrientation;
			CoordinatePlane cPlane = allCuts[0].cutOrientation;

			CoordinateAxis aNormal = aPlane.GetPlaneNormal();
			CoordinateAxis bNormal = bPlane.GetPlaneNormal();
			CoordinateAxis cNormal = cPlane.GetPlaneNormal();

			float[] aCutLocations = allCuts[2].cuts.Select(cut => cut.planeLocation).ToArray();
			float[] bCutLocations = allCuts[1].cuts.Select(cut => cut.planeLocation).ToArray();
			float[] cCutLocations = allCuts[0].cuts.Select(cut => cut.planeLocation).ToArray();
			
			List<VolumeInfo> volumes = new List<VolumeInfo>();
			for (int a = 1; a < aCutLocations.Length; a++) {
				for (int b = 1; b < bCutLocations.Length; b++) {
					for (int c = 1; c < cCutLocations.Length; c++) {
						CoordinateAxis spreadAxes = CoordinateAxis.XYZ;

						Vector3 a0 = Vector2.zero.ConstructPlanarVector3(aCutLocations[a - 1], aPlane);
						Vector3 a1 = Vector2.zero.ConstructPlanarVector3(aCutLocations[a    ], aPlane);
						Vector3 aCenter = (a0 + a1) * 0.5f;
						Vector3 aSize = a1 - a0;
						if (a == 1 || a == aCutLocations.Length - 1) {
							spreadAxes -= aNormal;
						}

						Vector3 b0 = Vector2.zero.ConstructPlanarVector3(bCutLocations[b - 1], bPlane);
						Vector3 b1 = Vector2.zero.ConstructPlanarVector3(bCutLocations[b    ], bPlane);
						Vector3 bCenter = (b0 + b1) * 0.5f;
						Vector3 bSize = b1 - b0;
						if (b == 1 || b == bCutLocations.Length - 1) {
							spreadAxes -= bNormal;
						}

						Vector3 c0 = Vector2.zero.ConstructPlanarVector3(cCutLocations[c - 1], cPlane);
						Vector3 c1 = Vector2.zero.ConstructPlanarVector3(cCutLocations[c    ], cPlane);
						Vector3 cCenter = (c0 + c1) * 0.5f;
						Vector3 cSize = c1 - c0;
						if (c == 1 || c == cCutLocations.Length - 1) {
							spreadAxes -= cNormal;
						}

						Vector3 center = aCenter + bCenter + cCenter;
						Vector3 size = aSize + bSize + cSize;

						Bounds bounds = new Bounds(center, size);

						VolumeInfo vi = new VolumeInfo(bounds, spreadAxes);

						volumes.Add(vi);
					}
				}
			}

			return volumes;
		}

		private void Update() {
			if (transform.hasChanged) {
				if (previousLocalScale != transform.localScale) {
					Vector3 newLocalScale = transform.localScale;
					ScaleChangedEvent?.Invoke(newLocalScale);
					previousLocalScale = newLocalScale;
				}
				transform.hasChanged = false;
			}
		}

		public struct VolumeInfo {
			public Bounds bounds;
			public CoordinateAxis spreadAxes;

			public VolumeInfo(Bounds bounds, CoordinateAxis spreadAxes) {
				this.bounds = bounds;
				this.spreadAxes = spreadAxes;
			}
		}

		[System.Serializable]
		public struct CutGroup {
			public CoordinatePlane cutOrientation;
			public PlaneInfo[] cuts;

			public Plane GetPlane(int cutIndex) {
				return cuts[cutIndex].ToPlane(cutOrientation);
			}
		}

		[System.Serializable]
		public struct PlaneInfo {
			public float planeLocation;

			public Plane ToPlane(CoordinatePlane orientation) {
				bool invertNormal = orientation == CoordinatePlane.XZ;

				Vector3 inPoint = Vector2.zero.ConstructPlanarVector3(planeLocation, orientation);

				Vector3 a = new Vector2(1, 0).ConstructPlanarVector3(0, orientation);
				Vector3 b = new Vector2(0, 1).ConstructPlanarVector3(0, orientation);
				Vector3 normal = Vector3.Cross(a.normalized, b.normalized);
				normal = invertNormal ? -normal : normal;

				return new Plane(normal, inPoint);
			}
		}

		public enum DrawMode { Sliced, Tiled }
		public enum TileMode { Continuous, Adaptive }
	}
}

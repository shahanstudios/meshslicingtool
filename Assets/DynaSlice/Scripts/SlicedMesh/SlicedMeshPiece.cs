using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Math = System.Math;

using DynaSlice.Utils;

namespace DynaSlice.Mesh {
	using Mesh = UnityEngine.Mesh;

	public abstract class SlicedMeshPiece : MonoBehaviour {
		protected Mesh pieceMesh;
		protected Material[] materials;
		protected new Transform transform;
		protected Bounds bounds;
		protected Bounds parentBounds;

		protected CoordinateAxis spreadAxes;
		protected CoordinateAxis leftoverCutNormals;

		protected Vector3 dirToParentCenter;
		protected Vector3 axialOffset;

		protected virtual void Start() {
			transform = GetComponent<Transform>();
			SlicedMesh parentSlicedMesh = GetComponentInParent<SlicedMesh>();
			parentBounds = parentSlicedMesh.BaseMeshBounds;

			axialOffset = (bounds.center - parentBounds.center).ApplyVectorAlongAxis(spreadAxes, 0).ConstructAxialVector3(spreadAxes);
			dirToParentCenter = GetDirectionToParentCenter();

			gameObject.name = $"{gameObject.name} : {spreadAxes}";

			PositionMeshPiece(dirToParentCenter);
			UpdatePieceScale(transform.parent.localScale);

			parentSlicedMesh.ScaleChangedEvent += UpdatePieceScale;
		}

		public virtual void Init(Mesh pieceMesh, Material[] materials, Bounds bounds, CoordinateAxis spreadAxes, List<CoordinatePlane> cutOrientations) {
			this.pieceMesh = pieceMesh;
			this.materials = materials;
			this.bounds = bounds;
			this.spreadAxes = spreadAxes;

			this.leftoverCutNormals = CoordinateAxis.XYZ - cutOrientations.Aggregate(0, (int cn, CoordinatePlane cp) => cn + (int)cp.GetPlaneNormal());
		}

		protected abstract void UpdatePieceScale(Vector3 scale);
		protected void UpdatePieceLocalScaleAndPosition(Vector3 scale, Vector3 appliedNewScale) {
			Vector3 invScale = scale.InvertComponents();
			transform.localScale = GetAppliedVectorComponents(appliedNewScale, invScale, true);

			Vector3 parentScaleFactor = -Vector3.Scale(dirToParentCenter, parentBounds.extents);
			Vector3 localScaledSize = Vector3.Scale(dirToParentCenter, Vector3.Scale(bounds.size, transform.localScale));
			transform.localPosition = parentScaleFactor + localScaledSize + Vector3.Scale(axialOffset, invScale);
		}

		private Vector3 GetDirectionToParentCenter() {
			float epsilon = 0.001f;
			Vector3 diff = parentBounds.center - bounds.center;
			diff = new Vector3(Mathf.Abs(diff.x) < epsilon ? 0 : diff.x, Mathf.Abs(diff.y) < epsilon ? 0 : diff.y, Mathf.Abs(diff.z) < epsilon ? 0 : diff.z);

			Vector3 dirToParentCenter = new Vector3(Math.Sign(diff.x), Math.Sign(diff.y), Math.Sign(diff.z));

			return GetAppliedVectorComponents(Vector3.zero, dirToParentCenter);
		}

		private void PositionMeshPiece(Vector3 dirToParentCenter) {
			Vector3 closestBoundsPointToCenter = new Vector3(
				GetBoundsMinOrMax(bounds, dirToParentCenter.x).x,
				GetBoundsMinOrMax(bounds, dirToParentCenter.y).y,
				GetBoundsMinOrMax(bounds, dirToParentCenter.z).z);

			Vector3[] nv = new Vector3[pieceMesh.vertexCount];
			for (int j = 0; j < nv.Length; j++) {
				nv[j] = pieceMesh.vertices[j] - closestBoundsPointToCenter;
			}
			pieceMesh.vertices = nv;
			pieceMesh.RecalculateBounds();
		}

		protected Vector3 GetSpaceToFill(Vector3 scale) {
			Vector3 scaledParentSize = Vector3.Scale(parentBounds.size, scale);
			Vector3 baseScaleLeftovers = parentBounds.size - bounds.size;

			Vector3 spaceToFill = scaledParentSize - baseScaleLeftovers;
			return spaceToFill;
		}

		private Vector3 GetBoundsMinOrMax(Bounds bounds, float sign) {
			if (sign > 0) {
				return bounds.max;
			}
			else if (sign < 0) {
				return bounds.min;
			}
			else {
				return bounds.center;
			}
		}

		protected Vector3 GetAppliedVectorComponents(Vector3 hasAxisFactor, Vector3 doesNotHaveAxisFactor, bool applyNormals = false) {
			bool checkFlag(CoordinateAxis flag) =>
				spreadAxes.HasFlag(flag) || (applyNormals && leftoverCutNormals.HasFlag(flag));

			return new Vector3(
				checkFlag(CoordinateAxis.X) ? hasAxisFactor.x : doesNotHaveAxisFactor.x,
				checkFlag(CoordinateAxis.Y) ? hasAxisFactor.y : doesNotHaveAxisFactor.y,
				checkFlag(CoordinateAxis.Z) ? hasAxisFactor.z : doesNotHaveAxisFactor.z
			);
		}
	}
}

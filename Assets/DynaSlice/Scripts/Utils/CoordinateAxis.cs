﻿using UnityEngine;

namespace DynaSlice.Utils {
	public enum CoordinateAxis { None, X, Y, XY, Z, XZ, YZ, XYZ }

	public static partial class CoordinateAxisExtensionMethods {
		public static CoordinatePlane[] GetCoordinatePlaneFromGivenNormal(this CoordinateAxis normalAxis) {
			switch (normalAxis) {
				case CoordinateAxis.X:
					return new CoordinatePlane[] { CoordinatePlane.YZ };
				case CoordinateAxis.Y:
					return new CoordinatePlane[] { CoordinatePlane.XZ };
				case CoordinateAxis.Z:
					return new CoordinatePlane[] { CoordinatePlane.XY };
				case CoordinateAxis.XY:
					return new CoordinatePlane[] { CoordinatePlane.YZ, CoordinatePlane.XZ };
				case CoordinateAxis.XZ:
					return new CoordinatePlane[] { CoordinatePlane.YZ, CoordinatePlane.XY };
				case CoordinateAxis.YZ:
					return new CoordinatePlane[] { CoordinatePlane.XZ, CoordinatePlane.XY };

				default:
					return new CoordinatePlane[0];
			}
		}
	}

	public static partial class VectorExtensionMethods {
		public static Vector2 ApplyVectorAlongAxis(this Vector2 vector, CoordinateAxis axis, float baseVal = 1) {
			return ((Vector3)vector).ApplyVectorAlongAxis(axis, baseVal);
		}
		public static Vector2 ApplyVectorAlongAxis(this Vector3 vector, CoordinateAxis axis, float baseVal = 1) {
			switch (axis) {
				case CoordinateAxis.X:
					return Vector2.up * baseVal + Vector2.right * vector.x;
				case CoordinateAxis.Y:
					return Vector2.up * baseVal + Vector2.right * vector.y;
				case CoordinateAxis.Z:
					return Vector2.up * baseVal + Vector2.right * vector.z;
				case CoordinateAxis.XY:
					return vector.XY();
				case CoordinateAxis.XZ:
					return vector.XZ();
				case CoordinateAxis.YZ:
					return vector.YZ();

				default:
					return Vector2.one * baseVal;
			}
		}

		public static Vector3 ConstructAxialVector3(this Vector2 vector, CoordinateAxis axis) {
			switch (axis) {
				case CoordinateAxis.X:
					return Vector3.right * vector.x;
				case CoordinateAxis.Y:
					return Vector3.up * vector.x;
				case CoordinateAxis.Z:
					return Vector3.forward * vector.x;
				case CoordinateAxis.XY:
					return new Vector3(vector.x, vector.y, 0);
				case CoordinateAxis.XZ:
					return new Vector3(vector.x, 0, vector.y);
				case CoordinateAxis.YZ:
					return new Vector3(0, vector.x, vector.y);

				default:
					return Vector3.zero;
			}
		}
	}
}

﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DynaSlice.Utils {
	public enum CoordinatePlane { YZ, XZ, XY }

	public static class CoordinatePlaneUtils {
		public static readonly IEnumerable<CoordinatePlane> CoordinatePlanes;

		static CoordinatePlaneUtils() {
			CoordinatePlanes = System.Enum.GetValues(typeof(CoordinatePlane)).Cast<CoordinatePlane>();
		}
	}

	public static partial class CoordinatePlaneExtensionMethods {
		public static CoordinateAxis GetPlaneNormal(this CoordinatePlane plane) {
			switch (plane) {
				case CoordinatePlane.XY:
					return CoordinateAxis.Z;
				case CoordinatePlane.XZ:
					return CoordinateAxis.Y;
				case CoordinatePlane.YZ:
					return CoordinateAxis.X;
				default:
					return CoordinateAxis.None;
			}
		}
	}

	public static partial class VectorExtensionMethods {
		public static Vector2 InPlane(this Vector3 vector, CoordinatePlane plane) {
			if (plane == CoordinatePlane.XY) {
				return vector.XY();
			}
			else if (plane == CoordinatePlane.XZ) {
				return vector.XZ();
			}
			else {
				return vector.YZ();
			}
		}
		public static float ValueNotInPlane(this Vector3 vector, CoordinatePlane plane) {
			if (plane == CoordinatePlane.XY) {
				return vector.z;
			}
			else if (plane == CoordinatePlane.XZ) {
				return vector.y;
			}
			else {
				return vector.x;
			}
		}
		public static Vector3 ConstructPlanarVector3(this Vector2 vector, float notInPlane, CoordinatePlane plane) {
			if (plane == CoordinatePlane.XY) {
				return new Vector3(vector.x, vector.y, notInPlane);
			}
			else if (plane == CoordinatePlane.XZ) {
				return new Vector3(vector.x, notInPlane, vector.y);
			}
			else {
				return new Vector3(notInPlane, vector.x, vector.y);
			}
		}
	}
}

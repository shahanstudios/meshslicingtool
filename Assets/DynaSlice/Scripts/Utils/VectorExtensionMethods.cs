﻿using UnityEngine;

namespace DynaSlice.Utils {
	public static partial class VectorExtensionMethods {
		public static Vector2 XY(this Vector3 vector) {
			return new Vector2(vector.x, vector.y);
		}
		public static Vector2 XZ(this Vector3 vector) {
			return new Vector2(vector.x, vector.z);
		}
		public static Vector2 YZ(this Vector3 vector) {
			return new Vector2(vector.y, vector.z);
		}

		public static Vector3 ScaleComponents(this Vector3 vector, Vector3 scaleBy) {
			return Vector3.Scale(vector, scaleBy);
		}
		public static Vector3 InvertComponents(this Vector3 vector) {
			return new Vector3(1 / vector.x, 1 / vector.y, 1 / vector.z);
		}
		public static Vector3 InverseScale(this Vector3 vector, Vector3 scaleBy) {
			return Vector3.Scale(vector, InvertComponents(scaleBy));
		}

		public static Vector2 ScaleComponents(this Vector2 vector, Vector2 scaleBy) {
			return Vector2.Scale(vector, scaleBy);
		}
		public static Vector2 InvertComponents(this Vector2 vector) {
			return new Vector2(1 / vector.x, 1 / vector.y);
		}
		public static Vector2 InverseScale(this Vector2 vector, Vector2 scaleBy) {
			return Vector2.Scale(vector, InvertComponents(scaleBy));
		}

		public static Vector3 ClampComponents(this Vector3 vector, Vector3 min, Vector3 max) {
			return new Vector3(
				Mathf.Clamp(vector.x, min.x, max.x),
				Mathf.Clamp(vector.y, min.y, max.y),
				Mathf.Clamp(vector.z, min.z, max.z)
			);
		}

		public static float MaxComponent(this Vector2 vector) {
			return Mathf.Max(vector.x, vector.y);
		}
		public static float MaxComponent(this Vector3 vector) {
			return Mathf.Max(Mathf.Max(vector.x, vector.y), vector.z);
		}
		public static float MinComponent(this Vector2 vector) {
			return Mathf.Min(vector.x, vector.y);
		}
		public static float MinComponent(this Vector3 vector) {
			return Mathf.Min(Mathf.Min(vector.x, vector.y), vector.z);
		}
	}
}
